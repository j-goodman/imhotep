/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	var imhotep = __webpack_require__(1);
	var keyEvents = __webpack_require__(2);
	
	
	window.onload = function () {
	  var canvas = document.getElementById("canvas");
	
	  var controlCube = new imhotep.Cube(
	    {x: 100, y: 100, z: 0}, {x: 200, y: 100, z: 0},
	    {x: 100, y: 200, z: 0}, {x: 200, y: 200, z: 0},
	
	    {x: 100, y: 100, z: 100}, {x: 200, y: 100, z: 100},
	    {x: 100, y: 200, z: 100}, {x: 200, y: 200, z: 100}
	  );
	
	keyEvents(document, controlCube);
	
	  setInterval(function() {
	    canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height);
	    controlCube.draw(canvas);
	  }, 32);
	};


/***/ },
/* 1 */
/***/ function(module, exports) {

	var imhotep = {
	
	  Cube: function (ptA, ptB, ptC, ptD, ptE, ptF, ptG, ptH)
	  //     e-------f
	  //    /|      /|
	  //   / |     / |
	  //  a--|----b  |
	  //  |  g----|--h
	  //  | /     | /
	  //  c-------d
	  // a POINT is an object with attributes x, y, and z
	  {
	    this.points = {
	      a: ptA,
	      b: ptB,
	      c: ptC,
	      d: ptD,
	      e: ptE,
	      f: ptF,
	      g: ptG,
	      h: ptH
	    };
	
	    this.vectorize = function (pos, vPt) {
	      var behind = (vPt.z-pos.z+1)/vPt.z;
	      var before = 1-behind;
	      var screenX = pos.x * behind + vPt.x * before;
	      var screenY = pos.y * behind + vPt.y * before;
	      return {screenX: screenX, screenY: screenY};
	    };
	
	    this.drawLine = function (ctx, fromPt, toPt) {
	      ctx.beginPath();
	      ctx.moveTo(fromPt.screenX, fromPt.screenY);
	      ctx.lineTo(toPt.screenX, toPt.screenY);
	      ctx.strokeStyle="#fc0";
	      ctx.stroke();
	    };
	
	    this.draw = function (canvas, vPt) {
	      var ctx = canvas.getContext('2d');
	
	      if (!vPt) {
	        // the default vanishing point is the center of the canvas, at a depth equal to its width
	        vPt = {x: canvas.width/2, y: canvas.height/2, z: canvas.width};
	      }
	
	      var screenPos = {};
	      ["a", "b", "c", "d", "e", "f", "g", "h"].forEach( function (pointName) {
	        screenPos[pointName] = this.vectorize({
	          x: this.points[pointName].x,
	          y: this.points[pointName].y,
	          z: this.points[pointName].z
	        }, vPt);
	      }.bind(this));
	
	      [
	        ["a", "b"], ["b", "d"], ["d", "c"], ["c", "a"],
	        ["e", "f"], ["f", "h"], ["h", "g"], ["g", "e"],
	        ["a", "e"], ["b", "f"], ["d", "h"], ["c", "g"]
	      ].forEach(function(lineTuple) {
	        var fromPt = screenPos[lineTuple[0]];
	        var toPt = screenPos[lineTuple[1]];
	        this.drawLine(ctx, fromPt, toPt);
	      }.bind(this));
	    };
	
	    this.move = function (dimension, increment) {
	      Object.keys(this.points).forEach(function (pointKey) {
	        this.points[pointKey][dimension] += increment;
	      }.bind(this));
	    }.bind(this);
	
	  },
	};
	
	module.exports = imhotep;


/***/ },
/* 2 */
/***/ function(module, exports) {

	var keyEvents = function (document, controlCube) {
	  document.onkeydown = function (e) {
	    switch(e.keyCode) {
	    case 39: //right
	      controlCube.move("x", 12);
	      break;
	    case 37: //left
	      controlCube.move("x", -12);
	      break;
	    case 38: //up
	      controlCube.move("y", -12);
	      break;
	    case 40: //down
	      controlCube.move("y", 12);
	      break;
	    case 16: //shift
	      controlCube.move("z", -12);
	      break;
	    case 32: //space
	      controlCube.move("z", 12);
	      break;
	    }
	  };
	};
	
	module.exports = keyEvents;


/***/ }
/******/ ]);
//# sourceMappingURL=bundle.js.map