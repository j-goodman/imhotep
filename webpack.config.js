module.exports = {
  entry: "./demo.js",
  output: {
    path: "./",
    filename: "bundle.js"
  },
  devtool: "source-map"
};
