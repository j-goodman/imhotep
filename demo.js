var imhotep = require('./imhotep.js');
var keyEvents = require('./keyEvents');


window.onload = function () {
  var canvas = document.getElementById("canvas");

  var controlCube = new imhotep.Cube(
    {x: 100, y: 100, z: 0}, {x: 200, y: 100, z: 0},
    {x: 100, y: 200, z: 0}, {x: 200, y: 200, z: 0},

    {x: 100, y: 100, z: 100}, {x: 200, y: 100, z: 100},
    {x: 100, y: 200, z: 100}, {x: 200, y: 200, z: 100}
  );

keyEvents(document, controlCube);

  setInterval(function() {
    canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height);
    controlCube.draw(canvas);
  }, 32);
};
