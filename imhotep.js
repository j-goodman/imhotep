var imhotep = {

  Cube: function (ptA, ptB, ptC, ptD, ptE, ptF, ptG, ptH)
  //     e-------f
  //    /|      /|
  //   / |     / |
  //  a--|----b  |
  //  |  g----|--h
  //  | /     | /
  //  c-------d
  // a POINT is an object with attributes x, y, and z
  {
    this.points = {
      a: ptA,
      b: ptB,
      c: ptC,
      d: ptD,
      e: ptE,
      f: ptF,
      g: ptG,
      h: ptH
    };

    this.vectorize = function (pos, vPt) {
      var behind = (vPt.z-pos.z+1)/vPt.z;
      var before = 1-behind;
      var screenX = pos.x * behind + vPt.x * before;
      var screenY = pos.y * behind + vPt.y * before;
      return {screenX: screenX, screenY: screenY};
    };

    this.drawLine = function (ctx, fromPt, toPt) {
      ctx.beginPath();
      ctx.moveTo(fromPt.screenX, fromPt.screenY);
      ctx.lineTo(toPt.screenX, toPt.screenY);
      ctx.strokeStyle="#fc0";
      ctx.stroke();
    };

    this.draw = function (canvas, vPt) {
      var ctx = canvas.getContext('2d');

      if (!vPt) {
        // the default vanishing point is the center of the canvas, at a depth equal to its width
        vPt = {x: canvas.width/2, y: canvas.height/2, z: canvas.width};
      }

      var screenPos = {};
      ["a", "b", "c", "d", "e", "f", "g", "h"].forEach( function (pointName) {
        screenPos[pointName] = this.vectorize({
          x: this.points[pointName].x,
          y: this.points[pointName].y,
          z: this.points[pointName].z
        }, vPt);
      }.bind(this));

      [
        ["a", "b"], ["b", "d"], ["d", "c"], ["c", "a"],
        ["e", "f"], ["f", "h"], ["h", "g"], ["g", "e"],
        ["a", "e"], ["b", "f"], ["d", "h"], ["c", "g"]
      ].forEach(function(lineTuple) {
        var fromPt = screenPos[lineTuple[0]];
        var toPt = screenPos[lineTuple[1]];
        this.drawLine(ctx, fromPt, toPt);
      }.bind(this));
    };

    this.move = function (dimension, increment) {
      Object.keys(this.points).forEach(function (pointKey) {
        this.points[pointKey][dimension] += increment;
      }.bind(this));
    }.bind(this);

  },
};

module.exports = imhotep;
