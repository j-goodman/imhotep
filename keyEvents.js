var keyEvents = function (document, controlCube) {
  document.onkeydown = function (e) {
    switch(e.keyCode) {
    case 39: //right
      controlCube.move("x", 12);
      break;
    case 37: //left
      controlCube.move("x", -12);
      break;
    case 38: //up
      controlCube.move("y", -12);
      break;
    case 40: //down
      controlCube.move("y", 12);
      break;
    case 16: //shift
      controlCube.move("z", -12);
      break;
    case 32: //space
      controlCube.move("z", 12);
      break;
    }
  };
};

module.exports = keyEvents;
